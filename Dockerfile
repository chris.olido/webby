FROM ubuntu:18.04
RUN apt-get -y update
RUN apt-get install -y apt-utils
RUN apt-get install -y apache2
EXPOSE 80
COPY index.html /var/www/html
CMD /usr/sbin/apache2ctl -D FOREGROUND
